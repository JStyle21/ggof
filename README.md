# Google Security Hall Of Fame <2010-2014

## Since google replaced the old security hall of fame in 2021, many contributions have been lost, this is a backup copy for anoyone looking for one.

 <a href="https://jstyle21.gitlab.io/ggof/hall-of-fame/archive/index.html" target="_blank">Open Hall Of Fame</a> 

##### The is as close to the original version as possible:
- All files except fonts are local
- Removed all scripts, this is pure html and css
- No dependenctis, no broken urls

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">To celebrate 10 years of <a href="https://twitter.com/Google?ref_src=twsrc%5Etfw">@google</a>&#39;s Vulnerability Rewards Programs, we are excited to announce the launch of our new platform: <a href="https://t.co/iBpWvPDcvl">https://t.co/iBpWvPDcvl</a>! <br><br>Learn more about the platform and enhancements to our VRP program here: <a href="https://t.co/cZmBCyt91c">https://t.co/cZmBCyt91c</a> <a href="https://t.co/Bs8xflx7ab">pic.twitter.com/Bs8xflx7ab</a></p>&mdash; Google VRP (Google Bug Hunters) (@GoogleVRP) <a href="https://twitter.com/GoogleVRP/status/1420006557329367040?ref_src=twsrc%5Etfw">July 27, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
